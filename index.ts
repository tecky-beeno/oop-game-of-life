let canvas = document.querySelector('canvas')
let context = canvas.getContext('2d')

class Box {
  alive: boolean
  nextAlive: boolean
  peerBoxList: Box[] = []

  constructor(public x: number, public y: number, public size: number) {
    this.x = this.x * this.size
    this.y = this.y * this.size
    this.alive = Math.random() < 0.5
  }

  calcNextTurn() {
    let alivePeer = 0
    for (let peerBox of this.peerBoxList) {
      if (peerBox.alive) {
        alivePeer++
      }
    }
    if (alivePeer < 2) {
      this.nextAlive = false
    } else if (alivePeer > 3) {
      this.nextAlive = false
    } else if (alivePeer == 3) {
      this.nextAlive = true
    } else {
      this.nextAlive = this.alive
    }
  }

  draw() {
    this.alive = this.nextAlive
    context.fillStyle = this.getColor()
    context.beginPath()
    context.rect(this.x, this.y, this.size, this.size)
    context.stroke()
    context.fill()
  }

  getColor() {
    if (this.alive) {
      return 'black'
    } else {
      return 'white'
    }
  }

  onClick() {
    this.alive = true
    this.nextAlive = true
  }
}

class World {
  // y -> x -> Box
  y_x_boxes: Box[][] = []
  // for looping all boxes
  all_boxes: Box[] = []

  // size = 300 * 3
  width = 300 * 2
  height = 300 * 2

  boxSize = 30

  isPause = false

  draw() {
    if (!this.isPause) {
      for (let box of this.all_boxes) {
        box.calcNextTurn()
      }
    }
    context.clearRect(0, 0, this.width, this.height)
    for (let box of this.all_boxes) {
      box.draw()
    }
  }

  addBox(x: number, y: number) {
    let box = new Box(x, y, this.boxSize)
    this.all_boxes.push(box)
    if (!this.y_x_boxes[y]) {
      this.y_x_boxes[y] = []
    }
    this.y_x_boxes[y][x] = box
  }

  start() {
    canvas.width = this.width
    canvas.height = this.height

    // create all boxes
    for (let y = 0; y < this.height / this.boxSize; y++) {
      for (let x = 0; x < this.width / this.boxSize; x++) {
        this.addBox(x, y)
      }
    }

    // find nearby boxes
    let N_Y = this.y_x_boxes.length
    for (let y = 0; y < N_Y; y++) {
      let x_boxes = this.y_x_boxes[y]
      let N_X = x_boxes.length
      for (let x = 0; x < x_boxes.length; x++) {
        let box = x_boxes[x]
        for (let dx of [-1, 0, 1]) {
          for (let dy of [-1, 0, 1]) {
            if (dx == 0 && dy == 0) {
              continue
            }
            let peerX = (x + dx + N_X) % N_X
            let peerY = (y + dy + N_Y) % N_Y
            let peerBox = this.y_x_boxes[peerY][peerX]
            box.peerBoxList.push(peerBox)
          }
        }
      }
    }

    // start repeating timer
    // setInterval(() => this.draw(), 1000 / 30)
    const loop = () => {
      this.draw()
      requestAnimationFrame(loop)
    }
    requestAnimationFrame(loop)
  }

  onClick(x: number, y: number) {
    let boxX = Math.floor(x / this.boxSize)
    let boxY = Math.floor(y / this.boxSize)
    let box = this.y_x_boxes[boxY][boxX]
    box.onClick()
  }

  pause() {
    this.isPause = true
  }

  resume() {
    this.isPause = false
  }
}

let world = new World()
world.start()

canvas.addEventListener('click', event => {
  let rect = canvas.getBoundingClientRect()
  let x = event.clientX - rect.left
  let y = event.clientY - rect.top
  world.onClick(x, y)
})

document.querySelector('#pause').addEventListener('click', () => world.pause())
document
  .querySelector('#resume')
  .addEventListener('click', () => world.resume())
